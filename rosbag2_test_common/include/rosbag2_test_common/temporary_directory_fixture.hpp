// Copyright 2018, Bosch Software Innovations GmbH.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ROSBAG2_TEST_COMMON__TEMPORARY_DIRECTORY_FIXTURE_HPP_
#define ROSBAG2_TEST_COMMON__TEMPORARY_DIRECTORY_FIXTURE_HPP_

#include <gmock/gmock.h>

#include <string>

#include "rcpputils/filesystem_helper.hpp"

using namespace ::testing;  // NOLINT

namespace rosbag2_test_common
{

class TemporaryDirectoryFixture : public Test
{
public:
  TemporaryDirectoryFixture()
  {
    char template_char[] = "tmp_test_dir.XXXXXX";
    char * dir_name = mkdtemp(template_char);
    temporary_dir_path_ = dir_name;
  }

  ~TemporaryDirectoryFixture() override
  {
    remove_directory_recursively(temporary_dir_path_);
  }
  void remove_directory_recursively(const std::string & directory_path)
  {
    DIR * dir = opendir(directory_path.c_str());
    if (!dir) {
      return;
    }
    struct dirent * directory_entry;
    while ((directory_entry = readdir(dir)) != nullptr) {
      // Make sure to not call ".." or "." entries in directory (might delete everything)
      if (strcmp(directory_entry->d_name, ".") != 0 && strcmp(directory_entry->d_name, "..") != 0) {
        if (directory_entry->d_type == DT_DIR) {
          remove_directory_recursively(directory_path + "/" + directory_entry->d_name);
        }
        remove((directory_path + "/" + directory_entry->d_name).c_str());
      }
    }
    closedir(dir);
    remove(temporary_dir_path_.c_str());
  }
  std::string temporary_dir_path_;
};

}  // namespace rosbag2_test_common

#endif  // ROSBAG2_TEST_COMMON__TEMPORARY_DIRECTORY_FIXTURE_HPP_
